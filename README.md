# AnnotateMed Guidelines

## How to install and run the project for Mac users

```bash
$ cp .env.example .env
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver
```
## How to install and run the project for Windows users

```bash
$ cp .env.example .env
$ python -m venv venv
$ cd venv
$ .\Scripts\activate
$ cd ..
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py runserver
```